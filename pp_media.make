api = 2
core = 7.x

;============================================
;  PP Media Projects
;============================================

projects[file_entity][type] = "module"
projects[file_entity][subdir] = "contrib"
projects[file_entity][download][type] = "git"
projects[file_entity][download][revision] = "3661d8b"
projects[file_entity][download][branch] = "7.x-2.x"

projects[media][version] = "2.0-alpha3"
projects[media][subdir] = "contrib"
projects[media][patch][1931336] = "http://drupal.org/files/1931336-d7-4.patch"
projects[media][patch][1934226] = "http://drupal.org/files/1934226-d7-2.patch"

projects[media_youtube][version] = "2.x-dev"
projects[media_youtube][subdir] = "contrib"
projects[media_youtube][download][type] = "git"
projects[media_youtube][download][revision] = "5faa00c"
projects[media_youtube][download][branch] = "7.x-2.x"

projects[media_vimeo][version] = "1.0-beta5"
projects[media_vimeo][subdir] = "contrib"
projects[media_vimeo][patch][1823078] = "http://drupal.org/files/1823078-1x-fix.patch"

projects[pp_media_library][type] = "module"
projects[pp_media_library][subdir] = "custom"
projects[pp_media_library][download][type] = "git"
projects[pp_media_library][download][url] = "http://git.drupal.org/sandbox/asgorobets/2156787.git"
projects[pp_media_library][download][revision] = "778e9a36222595a0586132cf33fff331edc1c682"